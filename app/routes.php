<?php


Route::any("/", "LocationController@map");
Route::any("/locations", "LocationController@index");
Route::any("/game", "GameController@index");
Route::any("/qr", "QRController@index");
Route::any("/admin", "AdminController@index");

Route::any("/location-{id}", array("as" => "location.details", "uses" => "LocationController@details"));
Route::any("/game-{id}", array("as" => "game.details", "uses" => "GameController@details"));
Route::any("/admin/edit-{id}", array("as" => "location.edit", "uses" => "AdminController@edit"));

Route::any("/game/enable", array("as" => "game.enable", "uses" => "GameController@enable"));
Route::any("/game/disable", array("as" => "game.disable", "uses" => "GameController@disable"));
Route::any("/game/reset", array("as" => "game.reset", "uses" => "GameController@reset"));
