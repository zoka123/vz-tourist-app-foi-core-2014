<?php

/**
 * Created by PhpStorm.
 * User: Zoka
 * Date: 28.05.14.
 * Time: 00:18
 */
class ImageTableSeeder extends \Illuminate\Database\Seeder
{

    public function run()
    {

        DB::table('images')->delete();

        $locations = Location::all(array("name", "id"))->toArray();

        $loc = array();
        foreach ($locations as $location) {
            $loc[$location["name"]] = $location["id"];
        }

        Image::create(
            array(
                "name" => "the_ritz_house_01.jpg",
                "location_id" => $loc['The Ritz House'],
            )
        );
        Image::create(
            array(
                "name" => "the_ritz_house_02.jpg",
                "location_id" => $loc['The Ritz House'],
            )
        );
        Image::create(
            array(
                "name" => "the_ritz_house_03.jpg",
                "location_id" => $loc['The Ritz House'],
            )
        );

        Image::create(
            array(
                "name" => "the_jesuit_monastery_01.jpg",
                "location_id" => $loc['The Jesuit Monastery'],
            )
        );
        Image::create(
            array(
                "name" => "the_jesuit_monastery_02.jpg",
                "location_id" => $loc['The Jesuit Monastery'],
            )
        );
        Image::create(
            array(
                "name" => "the_jesuit_monastery_03.jpg",
                "location_id" => $loc['The Jesuit Monastery'],
            )
        );

        Image::create(
            array(
                "name" => "the_franciscan_church_of_st_john_the_baptist_01.jpg",
                "location_id" => $loc['The Franciscan church of st. John the Baptist'],
            )
        );
        Image::create(
            array(
                "name" => "the_franciscan_church_of_st_john_the_baptist_02.jpg",
                "location_id" => $loc['The Franciscan church of st. John the Baptist'],
            )
        );
        Image::create(
            array(
                "name" => "the_franciscan_church_of_st_john_the_baptist_03.jpg",
                "location_id" => $loc['The Franciscan church of st. John the Baptist'],
            )
        );


        Image::create(
            array(
                "name" => "the_sermage_palace_01.jpg",
                "location_id" => $loc['The Sermage Palace'],
            )
        );
        Image::create(
            array(
                "name" => "the_sermage_palace_02.jpg",
                "location_id" => $loc['The Sermage Palace'],
            )
        );
        Image::create(
            array(
                "name" => "the_sermage_palace_03.jpg",
                "location_id" => $loc['The Sermage Palace'],
            )
        );

        Image::create(
            array(
                "name" => "the_old_town_01.jpg",
                "location_id" => $loc['The Old Town'],
            )
        );
        Image::create(
            array(
                "name" => "the_old_town_02.jpg",
                "location_id" => $loc['The Old Town'],
            )
        );
        Image::create(
            array(
                "name" => "the_old_town_03.jpg",
                "location_id" => $loc['The Old Town'],
            )
        );


        Image::create(
            array(
                "name" => "the_ursuline_convent_01.jpg",
                "location_id" => $loc['The Ursuline convent'],
            )
        );
        Image::create(
            array(
                "name" => "the_ursuline_convent_02.jpg",
                "location_id" => $loc['The Ursuline convent'],
            )
        );
        Image::create(
            array(
                "name" => "the_ursuline_convent_03.jpg",
                "location_id" => $loc['The Ursuline convent'],
            )
        );

        Image::create(
            array(
                "name" => "ursuline_church_of_the_birth_of_christ_01.jpg",
                "location_id" => $loc['Ursuline church of the birth of Christ'],
            )
        );
        Image::create(
            array(
                "name" => "ursuline_church_of_the_birth_of_christ_02.jpg",
                "location_id" => $loc['Ursuline church of the birth of Christ'],
            )
        );
        Image::create(
            array(
                "name" => "ursuline_church_of_the_birth_of_christ_03.jpg",
                "location_id" => $loc['Ursuline church of the birth of Christ'],
            )
        );

        Image::create(
            array(
                "name" => "the_country_palace_01.jpg",
                "location_id" => $loc['The Country palace'],
            )
        );
        Image::create(
            array(
                "name" => "the_country_palace_02.jpg",
                "location_id" => $loc['The Country palace'],
            )
        );
        Image::create(
            array(
                "name" => "the_country_palace_03.jpg",
                "location_id" => $loc['The Country palace'],
            )
        );
        Image::create(
            array(
                "name" => "the_patacic_palace_01.jpg",
                "location_id" => $loc['The Patačić Palace'],
            )
        );
        Image::create(
            array(
                "name" => "the_patacic_palace_02.jpg",
                "location_id" => $loc['The Patačić Palace'],
            )
        );
        Image::create(
            array(
                "name" => "the_patacic_palace_03.jpg",
                "location_id" => $loc['The Patačić Palace'],
            )
        );
        Image::create(
            array(
                "name" => "the_croatian_national_theatre_01.jpg",
                "location_id" => $loc['The Croatian National Theatre'],
            )
        );
        Image::create(
            array(
                "name" => "the_croatian_national_theatre_02.jpg",
                "location_id" => $loc['The Croatian National Theatre'],
            )
        );
        Image::create(
            array(
                "name" => "the_croatian_national_theatre_03.jpg",
                "location_id" => $loc['The Croatian National Theatre'],
            )
        );
        return;


    }

} 