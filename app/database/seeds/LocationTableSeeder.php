<?php
/**
 * Created by PhpStorm.
 * User: Zoka
 * Date: 28.05.14.
 * Time: 00:18
 */

class LocationTableSeeder extends \Illuminate\Database\Seeder {

    public function run(){
        DB::table('locations')->delete();

        Location::create(array(
                "name" => 'The Ritz House',
                "long_desc" => 'During the house\'s renewal, renaissance arches were found underneath the existing structure. Under the porch is a well-preserved stone entrance with a carved monogram IA - Andreas Italus de Argent, and the year 1540. He was a Varaždin goldsmith from the 16th century. The house is now named after the previous owner, who in the early part of the twentieth century had a coffee house, and the house remains one of the City\'s cafes. Due to frequent changes in the names of town square (on average every 20 years), many locals probably do not know its official name, but it is known to one and all as the \'Korzo\'. In the 16th century it became the center of town. It is certainly one of those places for which the motto "Vidjeti i biti viden" ("To see and to be seen") applies, and a very pleasant place to catch the first rays of spring sun on one of the terraces.',
                "short_desc" => 'This one-story angle house with a Renaissance arcature on the first floor, is one of the oldest houses in Varaždin. The stone lintel, beneath the porch with carved LA. monogram and the year 1540, has been preserved.',
                "lat" => 46.307937,
                "lng" => 16.33756,
                "main_image" => "the_ritz_house_01.jpg",
                "game_order" => 1,
            ));
        Location::create(array(
                "name" => 'The Jesuit Monastery',
                "long_desc" => 'A former Jesuit monastery erected at the end of the 17th century, later became a Paulin monastery. Nowdays, the monastery with the church, high school and seminary is the most valuable complex of the early Baroque arhitecture of Varaždin.',
                "lat" => 46.307832,
                "lng" => 16.337947,
                "main_image" => "the_jesuit_monastery_01.jpg",
                "game_order" => 2,
            ));
        Location::create(array(
                "name" => 'The Franciscan church of st. John the Baptist',
                "long_desc" => 'The church was built in 1650 on the site of a medieval church of Knight Hospitaliers Peter Rabba from Graz, was its constructor. The tower, erected in 1641, is the tallest in Varaždin (54,5m)',
                "lat" => 46.307779,
                "lng" => 16.336338,
                "main_image" => "the_franciscan_church_of_st_john_the_baptist_01.jpg",
                "game_order" => 3,
            ));
        Location::create(array(
                "name" => 'The Sermage Palace',
                "long_desc" => 'The harmonious Rococo palace takes the today appearance in mid 18th century. Its owner in the 17st century was baron Prassinzky and later the Sermage family.',
                "lat" => 46.309489,
                "lng" => 16.336317,
                "main_image" => "the_sermage_palace_01.jpg",
                "game_order" => 4,
            ));
        Location::create(array(
                "name" => 'The Old Town',
                "long_desc" => 'The construction of the fortress lasted from the end of the 13th century until mid 19th century. It was the main seat of Varaždin feudal families for hundreds of years. In the 16th century it gets high earthen walls and double water-filled ditches.',
                "lat" => 46.309929,
                "lng" => 16.33445,
                "main_image" => "the_old_town_01.jpg",
                "game_order" => 5,
            ));
        Location::create(array(
                "name" => 'The Ursuline convent',
                "long_desc" => 'The Ursuline sisters bought this house and the church grounds in the mid-18th century to build a convent and a girls school, marking the beginning of their long educational tradition.',
                "lat" => 46.30824,
                "lng" => 16.335293,
                "main_image" => "the_ursuline_convent_01.jpg",
                "game_order" => 6,
            ));
        Location::create(array(
                "name" => 'Ursuline church of the birth of Christ',
                "long_desc" => 'The Ursuline sisters arrived from Bratislava in 1703. at the invitation of the Drašković familiy. This single-nave baroque church was built in 1712, with the tower added in 1726. The tower is one of the most charming in the town',
                "lat" => 46.308625,
                "lng" => 16.335138,
                "main_image" => "ursuline_church_of_the_birth_of_christ_01.jpg",
                "game_order" => 7,
            ));
        Location::create(array(
                "name" => 'The Country palace',
                "long_desc" => 'It was erected in the 18th century for the Varaždin country by domestic constructor Jakob Erber. Damaged in the 1776 fire, it was renovated in a Classicist manner and remains such up to this day',
                "lat" => 46.307826,
                "lng" => 16.336523,
                "main_image" => "the_country_palace_01.jpg",
                "game_order" => 8,
            ));

        Location::create(array(
                "name" => 'The Patačić Palace',
                "long_desc" => 'This is the most beautiful Rococo palace in town, Today’s appearance dates to 1764 when the older house was renovated. It was home of the count Franjo Patačić and a center of social life of the then Varaždin.',
                "lat" => 46.307878,
                "lng" => 16.337291,
                "main_image" => "the_patacic_palace_01.jpg",
                "game_order" => 9,
            ));


        Location::create(array(
                "name" => 'The Croatian National Theatre',
                "long_desc" => 'The design for the historicist building of the Croatian National Theatre were made by the Viennese architect Hermann Helmer. The construction was entrusted to Zagreb master builder J. J. Jambrešćak. Aleksander Freudenreich made the designs for an extension executed in 1955.',
                "lat" => 46.30576,
                "lng" => 16.337747,
                "main_image" => "the_croatian_national_theatre_01.jpg",
                "game_order" => 10,
            ));


    }

} 