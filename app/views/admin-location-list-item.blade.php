<li class="location-list-item">
    <a href="{{ route("location.edit", $location->id) }}" class="clearfix">
    <?php $src = (isset($location->main_image) ? $location->main_image : "no-image.jpg"); ?>
    <img class="th" src="{{ asset("images/" . $src)}}">
    <h3>{{ $location -> name }}</h3>
    <p>{{ substr($location -> long_desc, 0, 100)}}...</p>

    </a>
</li>