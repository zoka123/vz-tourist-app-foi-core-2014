<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Varaždin tourist</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="{{ asset('assets/css/normalize.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/foundation.css') }}" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:400,500,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('assets/css/vz.css') }}" type="text/css">
    <script src="{{ asset('assets/js/vendor/modernizr.js') }}"></script>
</head>
<body>