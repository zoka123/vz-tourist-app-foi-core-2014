<ul class="no-bullet text-left">
    <li><a href="."><i class="fa fa-globe"></i> Varaždin map</a></li>
    <li><a href="{{ action("LocationController@index") }}"><i class="fa fa-map-marker"></i> Location list</a></li>
    <li><a href="{{ action("QRController@index") }}"><i class="fa fa-qrcode"></i> QR Scan</a></li>
    <li><a href="{{ action("GameController@index") }}"><i class="fa fa-gamepad"></i> Play Game</a></li>
</ul>