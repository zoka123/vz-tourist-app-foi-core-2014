@extends("base")

@section('left-sidebar')
@parent

@stop

@section('content')
<div class="row">
    <br/>
    @if ($game == false)
    <a href="{{route('game.enable')}}" class="button success expand">Enable game mode</a>
    @elseif ($game == true)
    <div class="row">
        <div class="small-6 column">
        <a href="{{route('game.disable')}}" class="button expand alert">Disable game mode</a>
        </div>
        <div class="small-6 column">
        <a href="{{route('game.reset')}}" class="button expand alert">Reset game</a>
        </div>
    </div>


    @if (!empty("quest"))
        <p class="panel text-center">{{ $quest }}</p>
    @endif

    <div id="map"></div>
    <button class="button locate expand"><i class="fa fa-map-marker"></i> Take me there</button>

    @endif




</div>

@stop

@section("custom-footer-scripts")
@parent

@if ($game == true)
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyCEyjkBxwRr81MoSdaFs_adORTj4_242wc"></script>
<script src="{{ asset('assets/js/gmaps.js') }}"></script>

<script>
    @if (isset($location))

    var map = new GMaps({
        div: '#map',
        lat: {{$location->lat}},
        lng: {{$location->lng}}
    });

    map.addMarker({
        lat: {{$location->lat}},
        lng: {{$location->lng}},
        icon: {
            size: new google.maps.Size(64, 64),
            url: '{{ asset('assets/img/pin.png') }}'
        }
        });

    $("button.locate").on("click", function(){
        takeMeThere();
    });

    @endif

    function takeMeThere()
    {
        if (navigator.geolocation)
        {
            $("button.locate i.fa").addClass("fa-spin");
            navigator.geolocation.getCurrentPosition(drawRouteTo, function(error){alert(error.message)}, {enableHighAccuracy: true});
        }
        else{alert("Geolocation is not supported by this browser.");}
    }

    function drawRouteTo(position)
    {
        var lat = {{ $location -> lat }};
        var lng = {{ $location -> lng }};


        map.drawRoute({
            origin: [position.coords.latitude, position.coords.longitude],
            destination: [lat, lng],
            travelMode: 'walking',
            strokeColor: '#C42121',
            strokeOpacity: 0.8,
            strokeWeight: 6
        });

        $("button.locate i.fa").removeClass("fa-spin");
    }





</script>
@endif

@stop