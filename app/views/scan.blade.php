@extends("base")

@section('left-sidebar')
@parent

<div class='select'>
    <label for='audioSource'>Audio source: </label><select id='audioSource'></select>
</div>

<div class='select'>
    <label for='videoSource'>Video source: </label><select id='videoSource'></select>
</div>

@stop

@section('content')
<video muted id="camsource" autoplay></video>
<script type="text/javascript" src="{{ asset('/assets/js/qr/starter.js')}}"></script>
<canvas id="qr-canvas" width="320" height="240" style="display:none"></canvas>

<h3 id="qr-value"></h3>

@stop

@section("custom-footer-scripts")
@parent
<script>
var game = false;

@if ($game)
game = true;

@endif
</script>
<script type="text/javascript" src="{{ asset('/assets/js/qr/lib/jsqrcode/grid.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/qr/lib/jsqrcode/version.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/qr/lib/jsqrcode/detector.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/qr/lib/jsqrcode/formatinf.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/qr/lib/jsqrcode/errorlevel.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/qr/lib/jsqrcode/bitmat.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/qr/lib/jsqrcode/datablock.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/qr/lib/jsqrcode/bmparser.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/qr/lib/jsqrcode/datamask.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/qr/lib/jsqrcode/rsdecoder.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/qr/lib/jsqrcode/gf256poly.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/qr/lib/jsqrcode/gf256.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/qr/lib/jsqrcode/decoder.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/qr/lib/jsqrcode/QRCode.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/qr/lib/jsqrcode/findpat.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/qr/lib/jsqrcode/alignpat.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/qr/lib/jsqrcode/databr.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/js/qr/qr.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/qr/camera.js') }}"></script>

@stop