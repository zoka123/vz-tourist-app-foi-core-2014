@extends("base")

@section('left-sidebar')
@parent

@stop

@section('content')
<div class="row">
    @if (isset($message))
    <div class="alert-box success">
        <span>{{$message}}</span>
    </div>
    @endif


    <div class="small-12 column">
    <h1>{{ $location -> name }}</h1>
    <p>lvl: {{ $location -> game_order }}</p>

    @if (isset($location->short_desc))
    <p>{{ $location->short_desc }}</p>
    @endif
    <hr>
    @if (isset($location->long_desc))
    <p>{{ $location->long_desc }}</p>
    @endif



    @if (isset($images))
    <ul class="small-block-grid-3 text-center clearing-thumbs" data-clearing>
    @foreach ($images as $image)
        <li><a href="{{ asset("images/".$image->name) }}"><img src="{{ asset("images/".$image->name) }}"  class="th"></a></li>
    @endforeach
    </ul>

    @endif

    <div id="map"></div>

        <button class="button locate expand"><i class="fa fa-map-marker"></i> Take me there</button>

    </div>
</div>
@stop

@section("custom-footer-scripts")
@parent
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyCEyjkBxwRr81MoSdaFs_adORTj4_242wc"></script>
<script src="{{ asset('assets/js/gmaps.js') }}"></script>
<script>

    var map = new GMaps({
        div: '#map',
        lat: {{$location->lat}},
        lng: {{$location->lng}}
    });

    <?php $img = '<img class="th th-small" src="'. asset("images/" . $location->main_image).'">';?>

    var contentString = '<div class="map-popup text-center clearfix">' +
        '<h3>{{$location->name}}</h3>'+
    @if (isset($location->main_image))
        '{{ $img }}' +
    @endif
    "</div>";
    map.addMarker({
        lat: {{$location->lat}},
        lng: {{$location->lng}},
        title: '{{$location->name}}',
        icon: {
            size: new google.maps.Size(64, 64),
            url: '{{ asset('assets/img/pin.png') }}'
        },
        click: function(e) {
        },
        infoWindow: {
            content: contentString
        }
    });

    function takeMeThere()
    {
        if (navigator.geolocation)
        {
            $("button.locate i.fa").addClass("fa-spin");
            navigator.geolocation.getCurrentPosition(drawRouteTo, function(error){alert(error.message)}, {enableHighAccuracy: true});
        }
        else{alert("Geolocation is not supported by this browser.");}
    }
    function drawRouteTo(position)
    {
        var lat = {{ $location -> lat }};
        var lng = {{ $location -> lng }};


        map.drawRoute({
            origin: [position.coords.latitude, position.coords.longitude],
            destination: [lat, lng],
            travelMode: 'walking',
            strokeColor: '#C42121',
            strokeOpacity: 0.8,
            strokeWeight: 6
        });

        $("button.locate i.fa").removeClass("fa-spin");
    }


    $("button.locate").on("click", function(){
        takeMeThere();
           return;

        navigator.geolocation.getCurrentPosition(
            function(position) {
                alert("Lat: " + position.coords.latitude + "\nLon: " + position.coords.longitude);
            },
            function(error){
                alert(error.message);
            }, {
                enableHighAccuracy: true
            }
        );


    })

    $(function(){

    })

</script>

@stop