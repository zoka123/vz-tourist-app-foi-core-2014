@extends("base")

@section('left-sidebar')
@parent

@stop

@section('content')
<div id="full-map">
</div>
@stop

@section("custom-footer-scripts")
@parent

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyCEyjkBxwRr81MoSdaFs_adORTj4_242wc"></script>
<script src="{{ asset('assets/js/gmaps.js') }}"></script>
<script src="{{ asset('assets/js/style.js') }}"></script>
<script>
    var map;

    function loadResults(items) {
        var items, markers_data = [];
        for (var i = 0; i < items.length; i++) {
            var item = items[i];

            if (item.lat != undefined && item.lng != undefined) {

                var contentString = '<a href="./location-'+item.id+'"><div class="map-popup clearfix"><h3 class="text-center">'+item.name+'</h3>';
                    if(item.main_image){
                        contentString = contentString + '<img class="left th th-small" src="./images/'+ item.main_image +'">';
                    }
                contentString = contentString + "<p>"+item.short_desc+'</p></div></a><a href="./location-'+item.id+'" class="button expand">Read more</a>';

                markers_data.push({
                    lat: item.lat,
                    lng: item.lng,
                    title: item.name,
                    icon: {
                        size: new google.maps.Size(64, 64),
                        url: '{{ asset('assets/img/pin.png') }}'
                    },
                    infoWindow: {
                        content: contentString
                    }
                });
            }
        }

        map.addMarkers(markers_data);
    }

    $(document).ready(function(){
        console.log(map_style);

        map = new GMaps({
            div: '#full-map',
            lat: 46.307832,
            lng: 16.337947
        });

        map.map.setOptions({styles: map_style});

        map.on('marker_added', function (marker) {
            var index = map.markers.indexOf(marker);
            if (index == map.markers.length - 1) {
                map.fitZoom();
            }
        });

        data = {{ $locations}};
        loadResults(data);

    });
</script>

@stop