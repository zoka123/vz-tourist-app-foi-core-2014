@extends("base")

@section('left-sidebar')
@parent

@stop

@section('content')
<div class="row">
    <p class="text-center"><img style="margin:20px" src="http://chart.apis.google.com/chart?cht=qr&chs=300x300&chl={{ $location->id }}&chld=H|0"></p>
{{ Form::model($location, array('route' => array('location.edit', $location->id))) }}

{{ Form::label('name', 'Name') }}
{{ Form::text('name') }}

{{ Form::label('long_desc', 'Long description') }}
{{ Form::text('long_desc') }}

{{ Form::label('short_desc', 'Short description') }}
{{ Form::text('short_desc') }}

{{ Form::label('lat', 'Lat') }}
{{ Form::text('lat') }}

{{ Form::label('lng', 'Lng') }}
{{ Form::text('lng') }}

{{ Form::label('main_image', 'Main image') }}
{{ Form::text('main_image') }}

{{ Form::submit('Update location!') }}
</div>


@stop

@section("custom-footer-scripts")
@parent
@stop