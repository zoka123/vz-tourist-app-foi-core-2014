@extends("base")

@section('left-sidebar')
@parent

@stop

@section('content')
<div class="row">
        <ul id="location-list" class="no-bullet">
            @foreach ($locations as $location)
            @include("admin-location-list-item", array("location" => $location))
            @endforeach
        </ul>
</div>
@stop

@section("custom-footer-scripts")
@parent
@stop