@include("includes.head")


<div class="off-canvas-wrap" data-offcanvas>
    <div class="inner-wrap">
        <nav class="tab-bar">
            <section class="left-small">
                <a class="left-off-canvas-toggle menu-icon" href="#"><span></span></a>
            </section>

            <section class="middle tab-bar-section">
                <a href="."><h1 class="title">Varaždin Tourist</h1></a>
            </section>
        </nav>

        <aside class="left-off-canvas-menu">
            <ul class="off-canvas-list">
                @section('left-sidebar')
                @include('includes.menu')
                @show

            </ul>
        </aside>

        <section class="main-section">
                @section('content')
                @show
        </section>

        <a class="exit-off-canvas"></a>

    </div>
</div>


</div>
@include("includes.footer-scripts")
@section("custom-footer-scripts")
@show
@include("includes.foot")