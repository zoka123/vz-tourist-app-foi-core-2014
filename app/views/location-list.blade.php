@extends("base")

@section('left-sidebar')
@parent

@stop

@section('content')
<div class="row">
        <ul id="location-list" class="no-bullet">

            @if(count($locations))

            @foreach ($locations as $location)
            @include("location-list-item", array("location" => $location))
            @endforeach
            @else
            <br/>
            <p class="text-center">Complete your quests to get all locations</p>
            @endif

        </ul>
</div>
@stop

@section("custom-footer-scripts")
@parent
@stop