<?php
/**
 * Created by PhpStorm.
 * User: Zoka
 * Date: 28.05.14.
 * Time: 00:18
 */

class Location extends Eloquent{

    protected $table = 'locations';
    protected $fillable = array("name","long_desc","short_desc","main_image","lat","lng","game_order");

    public function images(){
        return $this->hasMany("Image");
    }


} 