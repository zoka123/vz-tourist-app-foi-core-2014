<?php
/**
 * Created by PhpStorm.
 * User: Zoka
 * Date: 28.05.14.
 * Time: 00:18
 */

class Image extends Eloquent{

    protected $table = 'images';
    protected $fillable = array("name","location_id");

    public function location(){
        return $this->belongsTo("Location");
    }


} 