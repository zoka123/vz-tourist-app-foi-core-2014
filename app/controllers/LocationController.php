<?php
/**
 * Created by PhpStorm.
 * User: Zoka
 * Date: 28.05.14.
 * Time: 00:02
 */

class LocationController extends BaseController {

    private function getLocations(){
        $locations = null;
        if(GameController::getGameState()){
            $level = GameController::getCurrentLevel();
            $locations = Location::where("game_order","<=",$level)->get();
        } else {
            $locations = Location::get();
        }
        return $locations;
    }

    public function index(){
        $locations = $this->getLocations();
        return View::make("location-list", array("locations" => $locations));
    }

    public function details($id){
        $location = Location::find($id);
        if(!$location){
            return View::make("errors/no-location-error");
        }

        return View::make("location-details", array("location" => $location, "images" => $location->images()->get()));
    }

    public function map(){
        $locations = $this->getLocations();
        foreach($locations as $location){
            if(!isset($location->short_desc)){
                $location->short_desc = substr($location->long_desc,0,100)."...";
            }
        }


        return View::make("location-map", array("locations" => $locations));
    }

} 