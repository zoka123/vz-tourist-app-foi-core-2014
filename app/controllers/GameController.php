<?php

/**
 * Created by PhpStorm.
 * User: Zoka
 * Date: 28.05.14.
 * Time: 00:02
 */
class GameController extends BaseController
{

    public static $QUESTS = array(
        "This is some quest to solve, in order to get to the location 1",
        "This is some quest to solve, in order to get to the location 2",
        "This is some quest to solve, in order to get to the location 3",
        "This is some quest to solve, in order to get to the location 4",
        "This is some quest to solve, in order to get to the location 5",
        "This is some quest to solve, in order to get to the location 6",
        "This is some quest to solve, in order to get to the location 7",
        "This is some quest to solve, in order to get to the location 8",
        "This is some quest to solve, in order to get to the location 9",
        "This is some quest to solve, in order to get to the location 10",

    );

    public function index()
    {
        $game = self::getGameState();
        $level = self::getCurrentLevel();
        $quest = null;
        $location = null;

        if (isset($level) && array_key_exists($level, self::$QUESTS)) {
            $quest = self::$QUESTS[$level];

            $currentLevel = GameController::getCurrentLevel();
            $targetLevel = $currentLevel + 1;

            $location = Location::where("game_order","=",$targetLevel)->first();
        }

        return View::make("game", array("game" => $game, "quest" => $quest, "location" => $location));
    }

    public static function getGameState()
    {
        if (!isset($_COOKIE["game"])) {
            return false;
        }
        $game = $_COOKIE["game"];
        if (empty($game) || $game == "false") {
            $game = false;
        } elseif ($game == true) {
            $game = true;
        }

        return $game;
    }

    public static function getCurrentLevel()
    {
        if (!isset($_COOKIE["game_level"])) {
            return 0;
        }
        $game = $_COOKIE["game_level"];
        if (empty($game) || $game == "false") {
            $game = 0;
        }

        return $game;
    }

    public static function setLevel($level)
    {
        setcookie("game_level", $level, time() + (3600 * 24 * 30), "/");
    }

    public function enable()
    {
        setcookie("game", "true", time() + (3600 * 24 * 30), "/");

        return Redirect::action("GameController@index");
    }

    public function disable()
    {
        setcookie("game", "false", time() + (3600 * 24 * 30), "/");

        return Redirect::action("GameController@index");
    }

    public function reset()
    {
        setcookie("game", "", time() - 3600, "/");
        setcookie("game_level", "", time() - 3600, "/");

        return Redirect::action("GameController@index");
    }

    public function details($id)
    {
        $location = Location::find($id);
        $currentLevel = GameController::getCurrentLevel();
        $targetLevel = $currentLevel + 1;

        //echo "<!--Locatoin level {$location->game_order} and your level {$currentLevel} and target {$targetLevel}-->";
        $locLevel = $location->game_order;

        $message = null;

        if ($locLevel > $targetLevel) {
            return View::make("errors/bad-level-error");
        } else {
            if ($locLevel == $targetLevel) {
                GameController::setLevel($location->game_order);
                $message = "You have unlocked the next level!";
            }
        }

        if (!$location) {
            return View::make("errors/no-location-error");
        }

        return View::make(
            "location-details",
            array("location" => $location, "message" => $message, "images" => $location->images()->get())
        );
    }

} 