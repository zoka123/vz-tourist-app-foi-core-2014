<?php

class AdminController extends BaseController
{

    public function index()
    {
        $locations = Location::get();

        return View::make("admin-location-list", array("locations" => $locations));
    }

    public function edit($id)
    {
        $location = Location::find($id);

        if (Input::all()) {
            $location->fill(Input::all());
            $location->save();
            Session::flash("message",array("type"=>"success","text"=>"Location saved"));
        }

        return View::make("location-form", array("location" => $location));
    }

} 