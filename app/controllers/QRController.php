<?php
/**
 * Created by PhpStorm.
 * User: Zoka
 * Date: 28.05.14.
 * Time: 00:02
 */

class QRController extends BaseController {

    public function index(){
        $game = GameController::getGameState();
        return View::make("scan", array("game" => $game));
    }

} 