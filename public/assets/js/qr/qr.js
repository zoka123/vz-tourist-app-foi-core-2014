function read(a) {
    try {
        var id = parseInt(a);
        if (game) {
            window.location = "./game-" + id;

        } else {
            window.location = "./location-" + id;

        }
    } catch (e) {
        console.log(e);
    }
    $("#qr-value").text(a);
}

qrcode.callback = read;