/* JavaScript code */

navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;

var videoElement = document.querySelector("video");
var audioSelect = document.querySelector("select#audioSource");
var videoSelect = document.querySelector("select#videoSource");
var startButton = document.querySelector("button#start");



function gotSources(sourceInfos) {
	for (var i = 0; i != sourceInfos.length; ++i) {
		var sourceInfo = sourceInfos[i];
		var option = document.createElement("option");
		option.value = sourceInfo.id;
		if (sourceInfo.kind === 'audio') {
			option.text = sourceInfo.label || 'microphone ' + (audioSelect.length + 1);
			audioSelect.appendChild(option);
		} else if (sourceInfo.kind === 'video') {
			option.text = sourceInfo.label || 'camera ' + (videoSelect.length + 1);
			videoSelect.appendChild(option);
		} else {
			console.log('Some other kind of source: ', sourceInfo);
		}
	}
}

var cam_video_id = "camsource"

function start() {
	if (!!window.stream) {
		videoElement.src = null;
		window.stream.stop();
	}
	var audioSource = audioSelect.value;
	var videoSource = videoSelect.value;
	var constraints = {
		audio : {
			optional : [{
				sourceId : audioSource
			}]
		},
		video : {
			optional : [{
				sourceId : videoSource
			}]
		}
	};
	navigator.getUserMedia(constraints, successCallback, errorCallback);
}

function successCallback(stream) {
	window.stream = stream;
	// make stream available to console
	videoElement.src = window.URL.createObjectURL(stream);
	videoElement.play();

	cam = camera(cam_video_id);
	cam.start()

}

window.addEventListener('DOMContentLoaded', function() {
	return;

	if ( typeof MediaStreamTrack === 'undefined') {
		alert('This browser does not support MediaStreamTrack.\n\nTry Chrome Canary.');
	} else {
		MediaStreamTrack.getSources(gotSources);
	}

	// Assign the <video> element to a variable
	var video = document.getElementById(cam_video_id);
	var options = {
		"audio" : false,
		"video" : true
	};
	// Replace the source of the video element with the stream from the camera
	if (navigator.getUserMedia) {
		navigator.getUserMedia(options, function(stream) {
			video.src = (window.URL && window.URL.createObjectURL(stream)) || stream;
		}, function(error) {
			console.log(error)
		});
		// Below is the latest syntax. Using the old syntax for the time being for backwards compatibility.
		// navigator.getUserMedia({video: true}, successCallback, errorCallback);
	} else {
		$("#qr-value").text('Sorry, native web camera streaming (getUserMedia) is not supported by this browser...')
		return;
	}
}, false);

$(document).ready(function() {
	videoSelect.onchange = start;
	return;

	if (!navigator.getUserMedia)
		return;
	cam = camera(cam_video_id);
	cam.start()
})